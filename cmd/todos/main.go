package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"

	"todos/internal/handlers"
	"todos/internal/storage"
	"todos/internal/validators"
)

type Config struct {
	Port           int    `default:"3000"`
	DataSourceName string `required:"true"`
}

func main() {
	app := echo.New()
	app.Logger.SetLevel(log.INFO)
	app.Validator = validators.New()

	// Initialize configuration
	var config Config
	err := envconfig.Process("todos", &config)
	if err != nil {
		app.Logger.Fatal("message", "failed process config", "port", config.Port, "err", err)
		os.Exit(1)
	}

	// Connect to database
	db, err := sql.Open("postgres", config.DataSourceName)
	if err != nil {
		app.Logger.Fatal("message", "failed to open database connection", "error", err)
		os.Exit(1)
	}
	defer db.Close()

	if err := db.Ping(); err != nil {
		app.Logger.Fatal("message", "failed to ping database", "error", err)
		os.Exit(1)
	}

	// Setup metrics
	p := prometheus.NewPrometheus("todos", nil)
	p.Use(app)

	// Setup tracing
	c := jaegertracing.New(app, nil)
	defer c.Close()

	// Setup endpoints and handlers
	storage := storage.New(db)
	handler := handlers.New(storage)

	app.Use(middleware.Logger())
	app.GET("/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "OK")
	})
	app.GET("/ready", func(c echo.Context) error {
		return c.String(http.StatusOK, "OK")
	})

	api := app.Group("/api/v1")

	api.GET("/todos", handler.GetTodos)
	api.POST("/todos", handler.CreateTodo)
	api.GET("/todos/:todoId", handler.GetTodo)
	api.PUT("/todos/:todoId", handler.UpdateTodo)
	api.DELETE("/todos/:todoId", handler.DeleteTodo)

	api.GET("/todos/:todoId/comments", handler.GetComments)
	api.POST("/todos/:todoId/comments", handler.CreateComment)
	api.DELETE("/todos/:todoId/comments/:commentId", handler.DeleteComment)

	api.GET("/todos/:todoId/labels", handler.GetLabels)
	api.POST("/todos/:todoId/labels", handler.CreateLabel)
	api.DELETE("/todos/:todoId/labels/:labelId", handler.DeleteLabel)

	// Start server
	go func() {
		if err := app.Start(fmt.Sprintf(":%v", config.Port)); err != nil {
			app.Logger.Info("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := app.Shutdown(ctx); err != nil {
		app.Logger.Fatal(err)
	}
}
