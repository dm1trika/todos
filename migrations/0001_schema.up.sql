CREATE TABLE IF NOT EXISTS todos (
  id serial PRIMARY KEY,
  text varchar(280) NOT NULL,
  due_date timestamp without time zone NOT NULL,
  done boolean NOT NULL
);


CREATE TABLE IF NOT EXISTS labels (
  id serial PRIMARY KEY,
  todo_id integer REFERENCES todos(id) NOT NULL,
  text varchar(15) NOT NULL,
  color varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS comments (
  id serial PRIMARY KEY,
  todo_id integer REFERENCES todos(id) NOT NULL,
  text varchar(2000) NOT NULL
);
