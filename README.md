# Todo app

Here you can find todo app webserver which does the following:

- CRUD `Todos`
- CRD `Comments` related to `Todos`
- CRD `Labels` related to `Todos`
- Basic params and payload validation
- Health and ready checks
- Prometheus metrics
- Jaeger tracing support
- Swagger file in `/api/swagger.yaml`

## Build

```
$ go build cmd/todos/main.go
```

## Run
```
$ docker-compose up --build
```

## Test

```
$ go test ./...
```

## Deploy

There is `Dockerfile` for the application which can be used to deploy the app to services like now or heroku.
In the future we can enhance k8s configuration or deploy binary output. 

## Future improvements

- Integration tests with real db
- K8s configuration files
- Pagination for todo list
- Authentication\authorization
- Providing timeouts, throttling
- Joined response within todo, comments and labels
