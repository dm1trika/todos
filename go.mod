module todos

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.6
	github.com/labstack/gommon v0.3.0
	github.com/lib/pq v1.8.0
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/sys v0.0.0-20200817085935-3ff754bf58a9 // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
