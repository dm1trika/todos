FROM golang:1.13.4 AS builder

WORKDIR /build

# Copy and download dependencies
COPY go.mod go.sum ./
RUN go mod download

# Copy source code into the container.
COPY . .

# Compile binary (+flags)
ENV GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-w -s" -o todos ./cmd/todos/main.go

FROM scratch

# Copy over binary
COPY --from=builder ["/build/todos", "/"]

EXPOSE 80

ENTRYPOINT ["./todos"]
