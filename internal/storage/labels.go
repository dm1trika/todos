package storage

import (
	"todos/internal/types"
)

func (s *Storage) GetLabels(todoId int) (*[]types.Label, error) {
	rows, err := s.db.Query("SELECT * FROM labels where todo_id=$1", todoId)
	if err != nil {
		return nil, err
	}

	var labels []types.Label

	for rows.Next() {
		var (
			id     int
			todoId int
			text   string
			color  string
		)

		if err := rows.Scan(&id, &todoId, &text, &color); err != nil {
			return nil, err
		}

		labels = append(labels, types.Label{Id: id, TodoId: todoId, Text: text, Color: color})
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	if len(labels) == 0 {
		return nil, ErrNotFound
	}

	return &labels, nil
}

func (s *Storage) AddLabel(todoId int, text, color string) (int, error) {
	var labelId int

	err := s.db.QueryRow(`
		INSERT INTO labels(todo_id, text, color)
		VALUES($1, $2, $3)
		RETURNING id
	`, todoId, text, color).Scan(&labelId)

	return labelId, err
}

func (s *Storage) DeleteLabel(labelId int) error {
	_, err := s.db.Exec(`
		DELETE FROM labels WHERE id=$1
	`, labelId)

	return err
}
