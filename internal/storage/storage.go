package storage

import (
	"database/sql"
	"errors"
)

var ErrNotFound = errors.New("record is not found")

type Storage struct {
	db *sql.DB
}

func New(db *sql.DB) *Storage {
	return &Storage{db}
}
