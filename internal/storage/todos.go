package storage

import (
	"database/sql"
	"time"

	"todos/internal/types"
)

func (s *Storage) GetTodos() (*[]types.TodoStorage, error) {
	rows, err := s.db.Query("SELECT * FROM todos")
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}

		return nil, err
	}

	var todos []types.TodoStorage

	for rows.Next() {
		var (
			id      int
			text    string
			dueDate time.Time
			done    bool
		)

		if err := rows.Scan(&id, &text, &dueDate, &done); err != nil {
			return nil, err
		}

		todos = append(todos, types.TodoStorage{Id: id, Text: text, DueDate: dueDate, Done: done})
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	if len(todos) == 0 {
		return nil, ErrNotFound
	}

	return &todos, nil
}

func (s *Storage) GetTodo(todoId int) (*types.TodoStorage, error) {
	row := s.db.QueryRow("SELECT id, text, due_date, done FROM todos where id=$1", todoId)

	var todo types.TodoStorage

	if err := row.Scan(&todo.Id, &todo.Text, &todo.DueDate, &todo.Done); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}

		return nil, err
	}

	return &todo, nil
}

func (s *Storage) CreateTodo(text string, dueDate time.Time) (int, error) {
	var todoId int

	err := s.db.QueryRow(`
		INSERT INTO todos(text, due_date, done)
		VALUES($1, $2, $3)
		RETURNING id
	`, text, dueDate, false).Scan(&todoId)

	return todoId, err
}

func (s *Storage) UpdateTodo(todoId int, text string, dueDate time.Time, done bool) (*types.TodoStorage, error) {
	var todo types.TodoStorage

	row := s.db.QueryRow(`
		UPDATE todos
		SET text=$1, 
			due_date=$2, 
			done=$3
		WHERE id=$4
		RETURNING text, due_date, done
	`, text, dueDate, done, todoId)

	err := row.Scan(&todo.Text, &todo.DueDate, &todo.Done)

	return &todo, err
}

func (s *Storage) DeleteTodo(todoId int) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec(`
		DELETE FROM labels WHERE todo_id=$1
	`, todoId)
	if err != nil {
		tx.Rollback()

		return err
	}

	_, err = tx.Exec(`
		DELETE FROM comments WHERE todo_id=$1
	`, todoId)
	if err != nil {
		tx.Rollback()

		return err
	}

	_, err = tx.Exec(`
		DELETE FROM todos WHERE id=$1
	`, todoId)
	if err != nil {
		tx.Rollback()

		return err
	}

	err = tx.Commit()

	return err
}
