package storage

import (
	"todos/internal/types"
)

func (s *Storage) GetComments(todoId int) (*[]types.Comment, error) {
	rows, err := s.db.Query("SELECT * FROM comments where todo_id=$1", todoId)
	if err != nil {
		return nil, err
	}

	var comments []types.Comment

	for rows.Next() {
		var (
			id     int
			todoId int
			text   string
		)

		if err := rows.Scan(&id, &todoId, &text); err != nil {
			return nil, err
		}

		comments = append(comments, types.Comment{Id: id, TodoId: todoId, Text: text})
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	if len(comments) == 0 {
		return nil, ErrNotFound
	}

	return &comments, nil
}

func (s *Storage) AddComment(todoId int, text string) (int, error) {
	var id int

	err := s.db.QueryRow(`
		INSERT INTO comments(todo_id, text)
		VALUES($1, $2)
		RETURNING id
	`, todoId, text).Scan(&id)

	return id, err
}

func (s *Storage) DeleteComment(commentId int) error {
	_, err := s.db.Exec(`
		DELETE FROM comments WHERE id=$1
	`, commentId)

	return err
}
