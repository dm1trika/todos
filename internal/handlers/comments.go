package handlers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"

	"todos/internal/storage"
	"todos/internal/types"
)

func (h *Handler) GetComments(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	comments, err := h.storage.GetComments(todoId)
	if err != nil {
		if err == storage.ErrNotFound {
			return c.String(http.StatusNotFound, "couldn't find comments")
		}

		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, comments)
}

func (h *Handler) CreateComment(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	commentPayload := new(types.CommentPayload)
	if err := c.Bind(commentPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	if err := c.Validate(commentPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	commentId, err := h.storage.AddComment(todoId, commentPayload.Text)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, commentId)
}

func (h *Handler) DeleteComment(c echo.Context) error {
	commentId, err := strconv.Atoi(c.Param("commentId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "commentId must be a number")
	}

	err = h.storage.DeleteComment(commentId)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusNoContent, commentId)
}
