package handlers_test

import (
	"database/sql"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"todos/internal/handlers"
	"todos/internal/storage"
	"todos/internal/validators"
)

var dueDate, _ = time.Parse("2006-01-02", "2020-01-10")

func setupTest(t *testing.T) (*echo.Echo, *handlers.Handler, *sql.DB, sqlmock.Sqlmock) {
	app := echo.New()
	app.Validator = validators.New()

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	storage := storage.New(db)
	handler := handlers.New(storage)

	return app, handler, db, mock
}

func Test_GetTodos(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{
		"id",
		"text",
		"due_date",
		"done",
	}).
		AddRow("1", "Buy stuff", dueDate, true).
		AddRow("2", "Write tests", dueDate, false)

	mock.ExpectQuery("^SELECT (.+) FROM todos").WillReturnRows(rows)

	app.GET("/api/v1/todos", handler.GetTodos)
	request, _ := http.NewRequest("GET", "/api/v1/todos", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(
		t,
		"[{\"id\":1,\"text\":\"Buy stuff\",\"dueDate\":\"2020-01-10\",\"done\":true},{\"id\":2,\"text\":\"Write tests\",\"dueDate\":\"2020-01-10\",\"done\":false}]\n",
		bodyString,
	)
}

func Test_GetTodosNotFound(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{
		"id",
		"text",
		"due_date",
		"done",
	})

	mock.ExpectQuery("^SELECT (.+) FROM todos").WillReturnRows(rows)

	app.GET("/api/v1/todos", handler.GetTodos)
	request, _ := http.NewRequest("GET", "/api/v1/todos", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNotFound, recorder.Code)
}

func Test_GetTodo(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{
		"id",
		"text",
		"due_date",
		"done",
	}).
		AddRow("1", "Buy stuff", dueDate, true)

	mock.ExpectQuery("^SELECT (.+) FROM todos where id=(.+)").WillReturnRows(rows)

	app.GET("/api/v1/todos/:todoId", handler.GetTodo)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(
		t,
		"{\"id\":1,\"text\":\"Buy stuff\",\"dueDate\":\"2020-01-10\",\"done\":true}\n",
		bodyString,
	)
}

func Test_GetTodoNotFound(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{
		"id",
		"text",
		"due_date",
		"done",
	})

	mock.ExpectQuery("^SELECT (.+) FROM todos where id=(.+)").WillReturnRows(rows)

	app.GET("/api/v1/todos/:todoId", handler.GetTodo)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNotFound, recorder.Code)
}

func Test_GetTodoNotNumberParam(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.GET("/api/v1/todos/:todoId", handler.GetTodo)
	request, _ := http.NewRequest("GET", "/api/v1/todos/abc", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_CreateTodo(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id"}).AddRow("1337")
	mock.ExpectQuery("^INSERT INTO todos").WillReturnRows(rows)

	app.POST("/api/v1/todos", handler.CreateTodo)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("dueDate", "2020-01-10")

	request, _ := http.NewRequest("POST", "/api/v1/todos", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "1337\n", bodyString)
}

func Test_CreateTodoEmptyPayload(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos", handler.CreateTodo)

	data := url.Values{}

	request, _ := http.NewRequest("POST", "/api/v1/todos", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_CreateTodoNotIntDueDate(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos", handler.CreateTodo)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("dueDate", "tomorrow")

	request, _ := http.NewRequest("POST", "/api/v1/todos", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_CreateTodoTsDueDate(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos", handler.CreateTodo)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("dueDate", "123456")

	request, _ := http.NewRequest("POST", "/api/v1/todos", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_UpdateTodo(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{
		"text",
		"due_date",
		"done",
	}).
		AddRow("Buy stuff", dueDate, true)

	mock.ExpectQuery("^UPDATE todos").WithArgs("Buy stuff", dueDate, true, 999).WillReturnRows(rows)

	app.PUT("/api/v1/todos/:todoId", handler.UpdateTodo)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("dueDate", "2020-01-10")
	data.Set("done", "true")

	request, _ := http.NewRequest("PUT", "/api/v1/todos/999", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	recorder := httptest.NewRecorder()

	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "{\"id\":999,\"text\":\"Buy stuff\",\"dueDate\":\"2020-01-10\",\"done\":true}\n", bodyString)
}

func Test_UpdateNotNumberParam(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.PUT("/api/v1/todos/:todoId", handler.UpdateTodo)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("dueDate", "1597674349669")
	data.Set("done", "true")

	request, _ := http.NewRequest("PUT", "/api/v1/todos/abc", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	recorder := httptest.NewRecorder()

	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_UpdateEmptyPayload(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.PUT("/api/v1/todos/:todoId", handler.UpdateTodo)

	data := url.Values{}

	request, _ := http.NewRequest("PUT", "/api/v1/todos/999", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_UpdateTodoMissingField(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.PUT("/api/v1/todos/:todoId", handler.UpdateTodo)

	data := url.Values{}
	//data.Set("text", "Buy stuff")
	data.Set("dueDate", "1597674349669")
	data.Set("done", "true")

	request, _ := http.NewRequest("PUT", "/api/v1/todos/999", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_DeleteTodo(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	var todoId int64 = 999

	mock.ExpectBegin()
	mock.ExpectExec("^DELETE FROM labels").WithArgs(todoId).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^DELETE FROM comments").WithArgs(todoId).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^DELETE FROM todos").WithArgs(todoId).WillReturnResult(sqlmock.NewResult(todoId, 1))
	mock.ExpectCommit()

	app.DELETE("/api/v1/todos/:todoId", handler.DeleteTodo)
	request, _ := http.NewRequest("DELETE", "/api/v1/todos/999", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNoContent, recorder.Code)
	assert.Equal(t, "999\n", bodyString)
}

func Test_DeleteNotNumberParam(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.DELETE("/api/v1/todos/:todoId", handler.DeleteTodo)
	request, _ := http.NewRequest("DELETE", "/api/v1/todos/abc", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}
