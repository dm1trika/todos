package handlers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"

	"todos/internal/storage"
	"todos/internal/types"
)

const DueDateLayout = "2006-01-02"

func (h *Handler) GetTodos(c echo.Context) error {
	todos, err := h.storage.GetTodos()
	if err != nil {
		if err == storage.ErrNotFound {
			return c.String(http.StatusNotFound, "couldn't find todos")
		}

		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	if len(*todos) == 0 {
		return c.String(http.StatusNotFound, "couldn't find todos")
	}

	var todosRes []types.TodoResponse

	for _, todo := range *todos {
		todosRes = append(todosRes, types.TodoResponse{
			Id:      todo.Id,
			Text:    todo.Text,
			DueDate: todo.DueDate.Format(DueDateLayout),
			Done:    todo.Done,
		})
	}

	return c.JSON(http.StatusOK, todosRes)
}

func (h *Handler) GetTodo(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	todo, err := h.storage.GetTodo(todoId)
	if err != nil {
		if err == storage.ErrNotFound {
			return c.String(http.StatusNotFound, "couldn't find todo")
		}

		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, types.TodoResponse{
		Id:      todoId,
		Text:    todo.Text,
		DueDate: todo.DueDate.Format(DueDateLayout),
		Done:    todo.Done,
	})
}

func (h *Handler) CreateTodo(c echo.Context) error {
	todoPayload := new(types.TodoCreatePayload)
	if err := c.Bind(todoPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	if err := c.Validate(todoPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	dueDateISO, err := time.Parse(DueDateLayout, todoPayload.DueDate)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	todoId, err := h.storage.CreateTodo(todoPayload.Text, dueDateISO)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, todoId)
}

func (h *Handler) UpdateTodo(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	todoPayload := new(types.TodoPayload)

	if err = c.Bind(todoPayload); err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	if err := c.Validate(todoPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	dueDateISO, err := time.Parse(DueDateLayout, todoPayload.DueDate)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	todo, err := h.storage.UpdateTodo(todoId, todoPayload.Text, dueDateISO, todoPayload.Done)

	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, types.TodoResponse{
		Id:      todoId,
		Text:    todo.Text,
		DueDate: todo.DueDate.Format(DueDateLayout),
		Done:    todo.Done,
	})
}

func (h *Handler) DeleteTodo(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	err = h.storage.DeleteTodo(todoId)

	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusNoContent, todoId)
}
