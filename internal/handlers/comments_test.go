package handlers_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func Test_GetComments(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	commentsRows := sqlmock.NewRows([]string{
		"id",
		"todoId",
		"text",
	}).
		AddRow("1", "1", "my first comment")

	mock.ExpectQuery("^SELECT (.+) FROM comments").WillReturnRows(commentsRows)

	app.GET("/api/v1/todos/:todoId/comments", handler.GetComments)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/comments", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(
		t,
		"[{\"id\":1,\"todoId\":1,\"text\":\"my first comment\"}]\n",
		bodyString,
	)
}

func Test_GetCommentsNotFound(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	commentsRows := sqlmock.NewRows([]string{
		"id",
		"todoId",
		"text",
		"color",
	})

	mock.ExpectQuery("^SELECT (.+) FROM comments").WillReturnRows(commentsRows)

	app.GET("/api/v1/todos/:todoId/comments", handler.GetComments)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/comments", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNotFound, recorder.Code)
}

func Test_CreateComments(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id"}).AddRow("1337")

	mock.ExpectQuery("^INSERT INTO comments").WillReturnRows(rows)

	app.POST("/api/v1/todos/:todoId/comments", handler.CreateComment)

	data := url.Values{}
	data.Set("text", "Buy stuff")

	request, _ := http.NewRequest("POST", "/api/v1/todos/1/comments", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "1337\n", bodyString)
}

func Test_CreateCommentsEmptyText(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos/:todoId/comments", handler.CreateComment)

	data := url.Values{}
	data.Set("text", "")

	request, _ := http.NewRequest("POST", "/api/v1/todos/1/comments", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_CreateCommentsNotNumberParam(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos/:todoId/comments", handler.CreateComment)

	data := url.Values{}
	data.Set("text", "Buy stuff")

	request, _ := http.NewRequest("POST", "/api/v1/todos/abc/comments", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_DeleteComments(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	mock.ExpectExec("^DELETE FROM comments").WillReturnResult(sqlmock.NewResult(1, 1))

	app.GET("/api/v1/todos/:todoId/comments/:commentId", handler.DeleteComment)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/comments/1", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNoContent, recorder.Code)

}
