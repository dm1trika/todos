package handlers_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func Test_GetLabels(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	commentsRows := sqlmock.NewRows([]string{
		"id",
		"todoId",
		"text",
		"color",
	}).
		AddRow("1", "1", "amazing label", "yellow")

	mock.ExpectQuery("^SELECT (.+) FROM labels").WillReturnRows(commentsRows)

	app.GET("/api/v1/todos/:todoId/labels", handler.GetLabels)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/labels", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(
		t,
		"[{\"id\":1,\"todoId\":1,\"text\":\"amazing label\",\"color\":\"yellow\"}]\n",
		bodyString,
	)
}

func Test_GetLabelsNotFound(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	commentsRows := sqlmock.NewRows([]string{
		"id",
		"todoId",
		"text",
	})

	mock.ExpectQuery("^SELECT (.+) FROM labels").WillReturnRows(commentsRows)

	app.GET("/api/v1/todos/:todoId/labels", handler.GetLabels)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/labels", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNotFound, recorder.Code)
}

func Test_CreateLabels(t *testing.T) {
	app, handler, db, mock := setupTest(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id"}).AddRow("1337")

	mock.ExpectQuery("^INSERT INTO labels").WillReturnRows(rows)

	app.POST("/api/v1/todos/:todoId/labels", handler.CreateLabel)

	data := url.Values{}
	data.Set("text", "Buy stuff")
	data.Set("color", "Buy stuff")

	request, _ := http.NewRequest("POST", "/api/v1/todos/1/labels", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	recorder := httptest.NewRecorder()

	app.ServeHTTP(recorder, request)

	body, _ := ioutil.ReadAll(recorder.Body)
	bodyString := string(body)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "1337\n", bodyString)
}

func Test_CreateLabelsEmptyText(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos/:todoId/labels", handler.CreateLabel)

	data := url.Values{}
	data.Set("text", "")

	request, _ := http.NewRequest("POST", "/api/v1/todos/1/labels", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_CreateLabelsNotNumberParam(t *testing.T) {
	app, handler, db, _ := setupTest(t)
	defer db.Close()

	app.POST("/api/v1/todos/:todoId/labels", handler.CreateLabel)

	data := url.Values{}
	data.Set("text", "Buy stuff")

	request, _ := http.NewRequest("POST", "/api/v1/todos/abc/labels", strings.NewReader(data.Encode()))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusUnprocessableEntity, recorder.Code)
}

func Test_DeleteLabels(t *testing.T) {
	app, handler, _, mock := setupTest(t)

	mock.ExpectExec("^DELETE FROM labels").WillReturnResult(sqlmock.NewResult(1, 1))

	app.GET("/api/v1/todos/:todoId/labels/:labelId", handler.DeleteLabel)
	request, _ := http.NewRequest("GET", "/api/v1/todos/1/labels/1", nil)
	recorder := httptest.NewRecorder()
	app.ServeHTTP(recorder, request)

	assert.Nil(t, mock.ExpectationsWereMet())
	assert.Equal(t, http.StatusNoContent, recorder.Code)
}
