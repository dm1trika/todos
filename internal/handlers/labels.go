package handlers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"

	"todos/internal/storage"
	"todos/internal/types"
)

func (h *Handler) GetLabels(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	comments, err := h.storage.GetLabels(todoId)
	if err != nil {
		if err == storage.ErrNotFound {
			return c.String(http.StatusNotFound, "couldn't find labels")
		}

		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, comments)
}

func (h *Handler) CreateLabel(c echo.Context) error {
	todoId, err := strconv.Atoi(c.Param("todoId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "todoId must be a number")
	}

	labelPayload := new(types.LabelPayload)
	if err := c.Bind(labelPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	if err := c.Validate(labelPayload); err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	commentId, err := h.storage.AddLabel(todoId, labelPayload.Text, labelPayload.Color)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, commentId)
}

func (h *Handler) DeleteLabel(c echo.Context) error {
	labelId, err := strconv.Atoi(c.Param("labelId"))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, "labelId must be a number")
	}

	err = h.storage.DeleteLabel(labelId)
	if err != nil {
		c.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusNoContent, labelId)
}
