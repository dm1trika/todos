package handlers

import (
	"time"

	"todos/internal/types"
)

type Handler struct {
	storage Storage
}

func New(storage Storage) *Handler {
	return &Handler{storage}
}

type Storage interface {
	GetTodos() (*[]types.TodoStorage, error)
	GetTodo(todoId int) (*types.TodoStorage, error)
	CreateTodo(text string, dueDate time.Time) (int, error)
	UpdateTodo(todoId int, text string, dueDate time.Time, done bool) (*types.TodoStorage, error)
	DeleteTodo(todoId int) error

	GetComments(todoId int) (*[]types.Comment, error)
	AddComment(todoId int, text string) (int, error)
	DeleteComment(commentId int) error

	GetLabels(todoId int) (*[]types.Label, error)
	AddLabel(todoId int, text, color string) (int, error)
	DeleteLabel(labelId int) error
}
