package types

import "time"

type TodoStorage struct {
	Id      int
	Text    string
	DueDate time.Time
	Done    bool
}

type TodoCreatePayload struct {
	Text    string `validate:"required"`
	DueDate string `validate:"required"`
}

type TodoPayload struct {
	Text    string `validate:"required"`
	DueDate string `validate:"required"`
	Done    bool   `validate:"required"`
}

type TodoResponse struct {
	Id      int    `json:"id"`
	Text    string `json:"text"`
	DueDate string `json:"dueDate"`
	Done    bool   `json:"done"`
}

type CommentPayload struct {
	Text string `validate:"required"`
}

type Comment struct {
	Id     int    `json:"id" sql:"id"`
	TodoId int    `json:"todoId" sql:"todo_id"`
	Text   string `json:"text" sql:"text"`
}

type LabelPayload struct {
	Text  string `validate:"required"`
	Color string `validate:"required"`
}

type Label struct {
	Id     int    `json:"id" sql:"id"`
	TodoId int    `json:"todoId" sql:"todo_id"`
	Text   string `json:"text" sql:"text" query:"text"`
	Color  string `json:"color" sql:"color" query:"color"`
}
